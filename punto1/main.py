import numpy as np
import scipy.stats as st
import seaborn as sns
import matplotlib.pylab as plt
from estado import Estado


def get_conf_int_mean(n, mean, std, z_value=2.6):
    '''
    Calcula el intervalo de confianza. Formula:
        * desvio tipico = std / square_root(n)
        * d = z_value * desvio tipico
        return [X - d <= mu <= X + d ]
    '''
    d = z_value * ((std) / (n ** 0.5))
    return mean - d, mean + d


def turno_adicional(estado):
    estado.produccion_turno()
    estado.add_turno_adicional()
    estado.actualizar_costo_anual_de_stock()


def simulacion_diaria(estado):
    estado.produccion_turno()
    estado.calcular_demanda()
    estado.actualizar_inventario()
    if (estado.tiene_inventario_insuficiente()):
        turno_adicional(estado)


def simulacion_anual(estado):
    estado.inicializar_anio_laboral()
    for _ in range(estado.DIAS_HABILES):
        simulacion_diaria(estado)
    estado.calcular_costos_anuales()


def mostrar(estado):
    # calcular variables
    turnos_adicionales = estado.get_turnos_adicionales()
    costo_total_stock = estado.get_costo_total_stock()
    años_simulacion = len(costo_total_stock)
    ic_inferior, ic_superior = get_conf_int_mean(
        años_simulacion, np.mean(costo_total_stock), np.std(costo_total_stock)
    )
    ic_inf_turnos, ic_sup_turnos = get_conf_int_mean(
        len(turnos_adicionales),
        np.mean(turnos_adicionales),
        np.std(turnos_adicionales)
    )
    # show
    str_turnos = 'IC (99%) de turnos adicionales:'
    print(f'===> Simulacion con umbral stock: {estado.get_umbral_productos()}')
    print(f'Años: {estado.ANIOS_SIMULACIONES} de {estado.DIAS_HABILES} dias \
          --- \t Datos anuales:')
    print(f'{str_turnos} [{ic_inf_turnos:.2f},{ic_sup_turnos:.2f}]')
    print(f'Turnos Adicionales (mean): {np.mean(turnos_adicionales):.2f}')
    print(f'IC (99%) de costos [{ic_inferior:.2f}, {ic_superior:.2f}]')
    print(f'Costo stock (mean): {np.mean(costo_total_stock):.2f}')


def graficar(estado):
    # vars
    años = Estado.ANIOS_SIMULACIONES
    turnos_adicionales = estado.get_turnos_adicionales()
    costo_total_stock = estado.get_costo_total_stock()
    umbral = estado.get_umbral_productos()
    fig, ax = plt.subplots(ncols=2)
    # show
    ax[0].set_title(f'Años simulacion {años} - Umbral: {umbral} productos')
    ax[1].set_title(f'Años simulacion {años} - Umbral: {umbral} productos')
    ax[0].set(ylabel=f'Turnos Adicionales', xlabel="Años simulacion")
    ax[1].set(ylabel='Costo de Mantenimiento',
              xlabel="Años simulacion")
    xs = np.arange(1, años+1)
    ys1 = np.array(turnos_adicionales)[-30:]  # always show last 30
    ys2 = np.array(costo_total_stock)[-30:]
    # print(xs.shape, ys1.shape, ys2.shape)
    sns.barplot(x=xs, y=ys1, ax=ax[0])
    sns.barplot(x=xs, y=ys2, ax=ax[1])
    plt.show()


def show_results(estado):
    mostrar(estado)
    graficar(estado)


def simular(umbral=50):
    estado = Estado(umbral)
    for _ in range(estado.ANIOS_SIMULACIONES):
        simulacion_anual(estado)
    show_results(estado)


def main():
    simular()
    simular(umbral=60)
    simular(umbral=70)


if __name__ == "__main__":
    main()
