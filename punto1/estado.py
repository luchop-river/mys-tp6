import numpy as np
import scipy.stats as st
import seaborn as sns
import matplotlib.pylab as plt


class Estado(object):
    '''Representa el estado de la simulaicon'''
    ANIOS_SIMULACIONES = 30
    DIAS_HABILES = 250
    MAX_PODUCCION = 100
    COSTO_STOCK = 20
    INVENTARIO_INICIAL = 80
    _umbral_productos = 0
    inventario = 0
    cant_turnos_adicionales = 0
    costo_anual_stock = 0
    costo_total_stock = []
    turnos_adicionales = []

    def get_umbral_productos(self):
        return self._umbral_productos

    def get_costo_total_stock(self):
        return self.costo_total_stock

    def get_turnos_adicionales(self):
        return self.turnos_adicionales

    def tiene_inventario_insuficiente(self):
        return self.inventario < self.get_umbral_productos()

    def add_turno_adicional(self):
        self.cant_turnos_adicionales += 1

    def inicializar_anio_laboral(self):
        self.costo_anual_stock = 0
        self.cant_turnos_adicionales = 0

    def actualizar_costo_anual_de_stock(self):
        self.costo_anual_stock += (self.inventario * self.COSTO_STOCK)

    def produccion_turno(self):
        self.inventario += self.MAX_PODUCCION

    def calcular_demanda(self):
        self.demanda = np.random.normal(110, 20)
        return self.demanda

    def actualizar_inventario(self):
        self.inventario -= self.demanda

    def calcular_costos_anuales(self):
        self.turnos_adicionales.append(self.cant_turnos_adicionales)
        self.costo_total_stock.append(self.costo_anual_stock)

    def __init__(self, umbral=50):
        self.inventario = self.INVENTARIO_INICIAL
        self.costo_anual_stock = 0
        self._umbral_productos = umbral
