from pedido import Pedido
from actividad import Actividad


class Estado():

    INVENTARIO_INICIAL = 500
    COSTO_UNIDAD_FALTANTE = 625
    COSTO_MANTENIMIENTO_UNIDAD = 350
    COSTO_MANTENIMIENTO_UNIDAD_INVENTARIO_INICIAL = 1250
    PUNTO_REORDEN = 15

    def __init__(self):
        self.demanda_diaria = 0
        self.unidades_faltantes_por_dia = 0
        self.stock = self.INVENTARIO_INICIAL
        self.costo_mantenimiento_inventario = 0
        self.cantidad_mensual_de_ordenes = 0
        self.cantidad_mensual_de_unidades_faltantes = 0
        self.costos_mensuales_de_stock = []
        self.costos_mensuales_de_faltantes = []
        self.costos_mensuales_de_ordenes = []
        self.ordenes_de_pedidos = []
        self.ordenes_del_mes_siguiente = []
        self.actividad = Actividad(self)

    # getters atributos

    def get_demanda_diaria(self):
        return self.demanda_diaria

    # getters costos mensuales

    def get_costos_mensuales_de_stock(self):
        '''
        Retorna los costos mensuales de stock
        '''
        return self.costos_mensuales_de_stock

    def get_costos_mensuales_de_faltantes(self):
        '''
        Retorna los costos mensuales por faltantes
        '''
        return self.costos_mensuales_de_faltantes

    def get_costos_mensuales_de_ordenes(self):
        '''
        Retorna los costos mensuales de ordenes
        '''
        return self.costos_mensuales_de_ordenes

    # consultas atributos

    def cantidad_actual_de_faltantes_es_mayor_que(self, valor):
        '''
        Retorna boleando dependiendo si: unidades faltantes > valor recibido
        '''
        return self.unidades_faltantes_por_dia > valor

    def tengo_stock_para_soportar_demanda(self):
        '''
        Retorna boleando dependiendo si: stock >= demanda diaria
        '''
        return self.stock >= self.demanda_diaria

    # consultas de estado

    def __get_cantidad_total_de_unidades_en_pedidos_realizados(self):
        '''
        Retorna la cantidad de unidades solicitada en los pedidos que fueron
        realizados. Formula:
            cantidad de pedidos de ordenes * tamanio orden
        '''
        return len(self.ordenes_de_pedidos) * self.actividad.TAMANIO_ORDEN

    def debo_reordenar_por_faltantes(self):
        '''
        Retorna un booleand dependiendo de que la cantidad de unidades
        faltantes por dia > cantidad total de unidades existetes en
        los pedidos realizados
        '''
        return (self.unidades_faltantes_por_dia >
                self.__get_cantidad_total_de_unidades_en_pedidos_realizados()
                )

    def debo_reordenar_por_stock_menor_a_umbral(self):
        '''
        Retorna un booleano dependiedo que el stock sea menor al umbral y
        la cantidad total de unidades existetes en los pedidos realizados
        no supera la demanda
        '''
        return (self.stock <= self.PUNTO_REORDEN and self.demanda_diaria >
                self.__get_cantidad_total_de_unidades_en_pedidos_realizados())

    # setters

    def set_demanda_diaria(self, demanda):
        '''
        Setea la demanda diaria con un valor recibido
        '''
        self.demanda_diaria = demanda

    # add operations

    def add_nueva_orden_menusal(self):
        '''
        Incrementa la cantidad de ordenes mensuales
        '''
        self.cantidad_mensual_de_ordenes += 1

    def add_orden_nuevo_pedido(self, pedido):
        '''
        Recibe una orden y la encola en la lista de ordenes a resolver
        '''
        self.ordenes_de_pedidos.append(pedido)

    def add_orden_que_se_resolvera_el_mes_siguiente(self, orden):
        '''
        Recibe una orden y la encola en la lista de ordenes a resover
        el mes siguiente
        '''
        self.ordenes_del_mes_siguiente.append(orden)

    def add_cantidad_a_stock_mensual(self, cant_stock):
        '''
        Recibe un valor y se lo acumula al stock
        '''
        self.stock += cant_stock

    def substract_cantidad_stock_mensual(self, cant_stock):
        '''
        Recibe un valor y se lo resta al stock
        '''
        self.stock -= cant_stock

    def add_demanda_a_unidades_faltantes(self):
        '''
        Actualizo los valores de faltantes con respecto a la demadna actual:
            * Agrego la demanda diaria como faltante
            * Tambien actualizo la cantidad faltante mensual
        '''
        self.unidades_faltantes_por_dia += self.demanda_diaria
        self.cantidad_mensual_de_unidades_faltantes += self.demanda_diaria

    # actualizaciones

    def actualizar_stock_por_arribo_de_lotes(self, unidades_nuevas):
        '''
        Actualiza el stock por llegada de unidades que superan la cantidad
        de productos faltantes.
        '''
        unidades_disponibles = (unidades_nuevas -
                                self.unidades_faltantes_por_dia)
        self.add_cantidad_a_stock_mensual(unidades_disponibles)
        self.unidades_faltantes_por_dia = 0

    def actualizar_stock_siendo_suficiente_para_soportar_demanda(self):
        '''
        Actualiza el stock. Formula: stock -= demanda_diaria
        debe stock >= demanda_diaria
        '''
        # self.stock -= self.demanda_diaria
        self.substract_cantidad_stock_mensual(self.demanda_diaria)

    def actualizar_mantenimiento_productos_vendidos(self, dias_demora_entrega):
        '''
        Actualiza el costo de mantenimiento por prodcutos vendidos.
        Este costo representa el mantenimiento por papeleo de los productos
        formula: mantenimiento += unidades * costo_por_unidad * dias_a_guardar
        '''
        self.costo_mantenimiento_inventario += (
                self.COSTO_MANTENIMIENTO_UNIDAD *
                self.demanda_diaria * dias_demora_entrega
        )

    def actualizar_costo_diario_de_mantener_inventario(self):
        '''
        Actualiza el costo diario que conlleva mantener el stock
        formula: costo += stock * costo_por_unidad
        '''
        self.costo_mantenimiento_inventario += (self.stock *
                                                self.COSTO_MANTENIMIENTO_UNIDAD
                                                )

    # TODO:resolver ordenes_mes_siguiente al inicio el dia luego atender pedido

    def iniciar_mes(self):
        '''
        Realiza procesamiento necesario para comenzar la produccion mensual
        e inicializa los contadores a sus valores por defecto
        '''
        self.ordenes_de_pedidos = self.ordenes_del_mes_siguiente
        self.__reiniciar_costos_mensuales()

    def __reiniciar_costos_mensuales(self):
        '''
        Pone a cero los contadores mensuales de stock, faltantes y ordenes
        '''
        self.costo_mantenimiento_inventario = 0
        self.cantidad_mensual_de_ordenes = 0
        self.cantidad_mensual_de_unidades_faltantes = 0
        self.costo_mantenimiento_inventario = 0
        self.ordenes_del_mes_siguiente = []

    def calcular_costos_mensuales(self):
        '''
        Calcula los costos mensuales (guardandolos en listas).
        '''
        self.costos_mensuales_de_faltantes.append(
            self.cantidad_mensual_de_unidades_faltantes *
            self.COSTO_UNIDAD_FALTANTE
        )
        self.costos_mensuales_de_ordenes.append(
            self.cantidad_mensual_de_ordenes *
            Pedido.COSTO_ORDEN
        )
        self.costos_mensuales_de_stock.append(
            self.costo_mantenimiento_inventario
        )

    # actividades

    def escanear_actividades(self, dia, demanda, espera_lotes, demora_papeleo):
        '''
        Se realizan dos actividades Principales:
        (Al inicio del dia) Verifiacar llegada de lotes
        (Procesamiento) Atender demanda diaria
        (Al terminar dia) Verificar stock vs Umbral
        '''
        self.actividad.analizar_lotes(dia)
        self.actividad.atender_demanda_diaria(dia, demanda, espera_lotes,
                                              demora_papeleo)
        self.actividad.analizar_stock_diario_contra_umbral(dia, espera_lotes)
