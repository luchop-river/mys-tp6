class Pedido():
    COSTO_ORDEN = 3000
    DIAS_HABILES_MENSUALES = 30

    def __init__(self, inicio, demora):
        self.dia_inicio = inicio
        self.dias_demora = demora

    def llega_el_dia(self, dia):
        '''
        Retorna un booleano dependiendo de si el pedido ya llego o no
        dependiendo el dia que recibe por parametro
        '''
        return (dia >= (self.dia_inicio + self.dias_demora))

    def esta_en_espera(self, dia):
        '''
        Retorna un booleano dependiendo de si el pedido esta en espera o no
        dependiendo el dia que recibe por parametro
        '''
        return (dia < (self.dia_inicio + self.dias_demora))

    def puede_resolverse_antes_de_fin_de_mes(self):
        '''
        Retorna un booleano dependiendo si el pedido se puede resolver
        en el mes actual
        '''
        return ((self.dia_inicio + self.dias_demora) <
                self.DIAS_HABILES_MENSUALES)

    def resolver_parcialmente(self, dia_inicio_resolucion):
        '''
        Resuelve parcialmente el pedido, dejando la parte sin resolver para
        el mes que viene
        '''
        dias_resolucion = self.dia_inicio + self.dias_demora
        dias_laborales = self.DIAS_HABILES_MENSUALES - 1
        self.dia_inicio = 0
        self.dias_demora = (dias_resolucion - dias_laborales)
