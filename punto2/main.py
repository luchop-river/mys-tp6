import numpy as np
import scipy.stats as st
import seaborn as sns
import matplotlib.pyplot as plt
from estado import Estado


# Constantes


DIAS_HABILES = 30
MESES = 12


CASE_DEMANDA = (
    lambda x: 25 if (x < 0.2) else False,
    lambda x: 26 if (x < 0.24) else False,
    lambda x: 27 if (x < 0.30) else False,
    lambda x: 28 if (x < 0.42) else False,
    lambda x: 29 if (x < 0.62) else False,
    lambda x: 30 if (x < 0.68) else False,
    lambda x: 31 if (x < 0.83) else False,
    lambda x: 32 if (x < 0.93) else False,
    lambda x: 33 if (x < 0.98) else False,
    lambda x: 34,
)


CASE_DEMORA = (
    lambda x: 1 if (x < 0.4) else False,
    lambda x: 2 if (x < 0.6) else False,
    lambda x: 3 if (x < 0.75) else False,
    lambda x: 4 if (x < 0.9) else False,
    lambda x: 5,
)

CASE_ENTREGA = (
    lambda x: 1 if (x < 0.2) else False,
    lambda x: 2 if (x < 0.5) else False,
    lambda x: 3 if (x < 0.75) else False,
    lambda x: 4,
)


# Simulacion


def main():
    estado = Estado()
    for _ in range(MESES):
        simular_mes(estado)
    show(estado)


def simular_mes(estado):
    for dia in range(DIAS_HABILES):
        simular_dia(dia, estado)
    finalizar_mes(estado)


def show(estado):
    detallar(estado)
    graficar(estado)


def simular_dia(dia, estado):
    '''
    Realiza el escaneo de actividades principales:
        * atender la demanda diaria
        * si inventario < umbral && no hay ordenes realizadas then
            realizo pedido reorden
    '''
    estado.escanear_actividades(
        dia,
        get_demanda(),
        get_tiempo_demora_lote(),
        get_tiempo_entrega_cliente()
    )


def finalizar_mes(estado):
    '''
    Calcula costos mensuales e inicializa estado para iniciar un nuevo mes
    '''
    estado.calcular_costos_mensuales()
    estado.iniciar_mes()


def detallar(estado):
    # vars
    faltantes = estado.get_costos_mensuales_de_faltantes()
    orden = estado.get_costos_mensuales_de_ordenes()
    stock = estado.get_costos_mensuales_de_stock()
    total = get_total(faltantes, orden, stock)
    # values
    ic_inf_f, ic_sup_f = get_conf_int_mean(faltantes)
    ic_inf_o, ic_sup_o = get_conf_int_mean(orden)
    ic_inf_s, ic_sup_s = get_conf_int_mean(stock)
    ic_inf_t, ic_sup_t = get_conf_int_mean(total)
    print('==> Costos totales (anual)')
    print(f'Faltante (mean) {np.mean(faltantes):.2f} \
        -> IC (95%) =  [{ic_inf_f:.2f},{ic_sup_f:.2f}]')
    print(f'Orden (mean) {np.mean(orden):.2f} \
        -> IC (95%) = [{ic_inf_o:.2f},{ic_sup_o:.2f}]')
    print(f'Stock (mean) {np.mean(stock):.2f} \
        -> IC (95%) = [{ic_inf_s:.2f},{ic_sup_s:.2f}]')
    print(f'Total (mean){np.mean(total):.2f} \
        -> IC (95%) = [{ic_inf_t:.2f},{ic_sup_t:.2f}]')


def graficar(estado):
    total = get_total(
        estado.get_costos_mensuales_de_faltantes(),
        estado.get_costos_mensuales_de_ordenes(),
        estado.get_costos_mensuales_de_stock()
    )
    # show
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(14, 5))
    ax[0, 0].set(ylabel=f'Cotos de stock', xlabel="Meses de simulación")
    ax[0, 1].set(ylabel=f'Cotos faltante', xlabel="Meses de simulación")
    ax[1, 0].set(ylabel=f'Cotos de ordenes', xlabel="Meses de simulación")
    ax[1, 1].set(ylabel=f'Cotos Totales', xlabel="Meses de simulación")

    xs = np.arange(1, MESES+1)
    ys1 = np.array(estado.get_costos_mensuales_de_stock())
    ys2 = np.array(estado.get_costos_mensuales_de_faltantes())
    ys3 = np.array(estado.get_costos_mensuales_de_ordenes())
    ys4 = np.array(total)
    sns.barplot(x=xs, y=ys1, ax=ax[0, 0])
    sns.barplot(x=xs, y=ys2, ax=ax[0, 1])
    sns.barplot(x=xs, y=ys3, ax=ax[1, 0])
    sns.barplot(x=xs, y=ys4, ax=ax[1, 1])
    fig.tight_layout()
    plt.show()


def get_total(faltantes, orden, stock):
    total = []
    for i in range(MESES):
        val = (faltantes[i] + orden[i] + stock[i])
        total.append(val)
    return total


# def get_conf_int_mean(n, mean, std, z_value=1.96):
def get_conf_int_mean(a, z_value=1.96):
    '''
    Calcula el intervalo de confianza. Formula:
        * desvio tipico = std / square_root(n)
        * d = z_value * desvio tipico
        return [X - d <= mu <= X + d ]
    '''
    n, mean, std = len(a), np.mean(a), np.std(a)
    d = z_value * ((std) / (n ** 0.5))
    return mean - d, mean + d


def get_demanda():
    '''
    Calcular una cantidad aleatoria de pedidos diarios.
    '''
    value = np.random.uniform(0, 1)
    return get_valor(value, CASE_DEMANDA)


def get_tiempo_demora_lote():
    '''
    Calcula el tiempo de demora entre [1..5] aleatoriamente que llegue
    el nuevo lote
    '''
    value = np.random.uniform(0, 1)
    return get_valor(value, CASE_DEMORA)


def get_tiempo_entrega_cliente():
    '''
    Calcula el tiempo de entrega en dias [1..4]
    que lleva el papeleo para entragar un producto
    '''
    value = np.random.uniform(0, 1)
    return get_valor(value, CASE_ENTREGA)


def get_valor(x, func_list):
    for f in func_list:
        y = f(x)
        if y:
            break
    return y


if __name__ == "__main__":
    main()
