from actividad import Actividad
from pedido import Pedido


class Estado():

    def __init__(self, umbral):
        # constantes
        self.COSTO_UNITARIO_DEMANDA = 50
        self.COSTO_DIARIO_DE_ALMACENAR_PRODUCTOS_POR_UNIDAD = 15
        self.COSTO_UNITARIO_FALTANTE = 20
        # variables utilizacion diaria
        self.__umbral = umbral
        self.demanda = 0
        self.stock = 0
        self.unidades_faltantes = 0
        # ordenes de pedidos
        self.ordenes_de_pedidos = []
        self.ordenes_que_llegaran_el_mes_siguiente = []
        # acumulados mensuales
        self.acumulado_mensual_de_demandas = 0
        self.acumulado_mensual_de_unidades_faltantes = 0
        self.acumulado_mensual_de_ordenes_realizadas = 0
        self.acumulado_mensual_de_mantener_productos_almacenados = 0
        # listas con acumulados menusales
        self.costos_mensuales_de_demandas = []
        self.costos_mensuales_de_faltantes = []
        self.costos_mensuales_de_ordenes = []
        self.costos_mensuales_de_mantener_productos_almacenados = []
        # actividad
        self.actividad = Actividad(self)

    # getters costos mensuales

    def get_costos_mensuales_de_almacenar_productos(self):
        '''
        Retorna los costos mensuales de mantener almacenado el stock
        '''
        return self.costos_mensuales_de_mantener_productos_almacenados

    def get_costos_mensuales_de_faltantes(self):
        '''
        Retorna los costos mensuales por faltantes
        '''
        return self.costos_mensuales_de_faltantes

    def get_costos_mensuales_de_ordenes(self):
        '''
        Retorna los costos mensuales de ordenes
        '''
        return self.costos_mensuales_de_ordenes

    # setters

    def set_demanda_diaria(self, demanda):
        '''
        Setea la demanda con un valor que recibe como parametro
        '''
        self.demanda = demanda

    def add_cantidad_a_stock_mensual(self, cantidad):
        '''
        Recibe un valor y se lo acumula al stock
        '''
        self.stock += cantidad

    def add_orden_que_llegara_el_mes_siguiente(self, orden):
        '''
        Encola una orden que fue pedida el mes actual, pero que llegara
        el mes siguiente
        '''
        self.ordenes_que_llegaran_el_mes_siguiente.append(orden)

    def add_nueva_orden_menusal(self):
        '''
        Incrementa el total de ordenes mensuales realizadas
        '''
        self.acumulado_mensual_de_ordenes_realizadas += 1

    def add_nuevo_pedido_de_orden_mensual(self, pedido):
        '''
        Agrega un nuevo pedido a la lista de ordenes mensuales
        '''
        self.ordenes_de_pedidos.append(pedido)

    def decrementar_unidades_faltantes(self, cantidad):
        '''
        Decrementa la cantidad de unidades faltantes
        '''
        self.unidades_faltantes -= cantidad

    # consultas

    def tengo_stock_para_soportar_demanda(self):
        '''
        Retorna boleando dependiendo si: stock >= demanda diaria
        '''
        return self.stock >= self.demanda

    def cantidad_actual_de_faltantes_es_mayor_que(self, valor):
        '''
        Retorna boleando dependiendo si: unidades faltantes > valor recibido
        '''
        return self.unidades_faltantes > valor

    def debo_reordenar_por_stock_menor_a_umbral(self):
        '''
        Retorna un booleano dependiedo que el stock sea menor al umbral y
        la cantidad total de unidades existetes en los pedidos realizados
        no supera la demanda
        '''
        return (self.stock <= self.__umbral and self.demanda >
                self.__get_cantidad_total_de_unidades_en_pedidos_realizados())

    def debo_reordenar_por_faltantes(self):
        '''
        Retorna un booleano dependiendo de que la cantidad de unidades
        faltantes por dia > cantidad total de unidades existetes en
        los pedidos realizados
        '''
        return (self.unidades_faltantes >
                self.__get_cantidad_total_de_unidades_en_pedidos_realizados()
                )

    def __get_cantidad_total_de_unidades_en_pedidos_realizados(self):
        '''
        Retorna la cantidad de unidades solicitada en los pedidos que fueron
        realizados. Formula:
            cantidad de pedidos de ordenes * tamanio orden
        '''
        return len(self.ordenes_de_pedidos) * self.actividad.TAMANIO_DE_ORDEN

    # actualizacios de variables

    def add_demanda_a_unidades_faltantes(self):
        '''
        Actualizo los valores de faltantes con respecto a la demadna actual:
            * Agrego la demanda diaria como faltante
            * Tambien actualizo la cantidad faltante mensual
        '''
        self.unidades_faltantes += self.demanda
        self.acumulado_mensual_de_unidades_faltantes += self.demanda

    def actualizar_stock_por_arribo_de_lotes(self, unidades_nuevas):
        '''
        Actualiza el stock por llegada de unidades que superan la cantidad
        de productos faltantes.
        '''
        unidades_disponibles = (unidades_nuevas -
                                self.unidades_faltantes)
        # self.add_cantidad_a_stock_mensual(unidades_disponibles)
        self.stock += unidades_disponibles
        self.unidades_faltantes = 0

    # actualizaciones de estado

    def actualizar_stock_siendo_suficiente_para_soportar_demanda(self):
        '''
        Actualiza el stock. Formula: stock -= demanda_diaria
        '''
        self.stock -= self.demanda

    def actualizar_costo_de_productos_vendidos(self):
        '''
        Actualiza el costo de mantenimiento por prodcutos vendidos.
        formula: costo_por_demandas += unidades_demandadas * costo_por_unidad
        '''
        self.acumulado_mensual_de_demandas += (
                self.COSTO_UNITARIO_DEMANDA *
                self.demanda
        )

    def actualizar_costo_diario_de_mantener_inventario(self):
        '''
        Actualiza el costo diario que conlleva mantener el stock
        formula: costo += stock * costo_por_unidad
        '''
        self.acumulado_mensual_de_mantener_productos_almacenados += (
            self.stock * self.COSTO_DIARIO_DE_ALMACENAR_PRODUCTOS_POR_UNIDAD)

    # Actividades

    def iniciar_mes(self):
        '''
        Realiza procesamiento necesario para comenzar la produccion mensual
        e inicializa los contadores a sus valores por defecto
        '''
        self.ordenes_de_pedidos = self.ordenes_que_llegaran_el_mes_siguiente
        self.__reiniciar_costos_mensuales()

    def __reiniciar_costos_mensuales(self):
        '''
        Pone a cero los contadores mensuales de stock, faltantes y ordenes
        '''
        self.acumulado_mensual_de_demandas = 0
        self.acumulado_mensual_de_mantener_productos_almacenados = 0
        self.acumulado_mensual_de_ordenes_realizadas = 0
        self.acumulado_mensual_de_unidades_faltantes = 0
        self.ordenes_que_llegaran_el_mes_siguiente = []

    def calcular_costos_mensuales(self):
        '''
        Calcula los costos mensuales (guardandolos en listas).
        '''
        self.costos_mensuales_de_faltantes.append(
            self.acumulado_mensual_de_unidades_faltantes *
            self.COSTO_UNITARIO_FALTANTE
        )
        self.costos_mensuales_de_ordenes.append(
            self.acumulado_mensual_de_ordenes_realizadas *
            Pedido.COSTO_UNITARIO_ORDEN
        )
        self.costos_mensuales_de_mantener_productos_almacenados.append(
            self.acumulado_mensual_de_mantener_productos_almacenados
        )
        self.costos_mensuales_de_demandas.append(
            self.acumulado_mensual_de_demandas
        )

    # escaneo

    def escanear_actividades(self, dia, demanda):
        '''
        Escaneo de actividades:
            * procesar posible llegada de lotes
            * atender demanda diaria
            * analizar stock vs umbral
        '''
        self.actividad.analizar_llegada_de_lotes(dia)
        self.actividad.atender_demanda_diaria(dia, demanda)
        self.actividad.analizar_stock_diario_contra_umbral(dia)
