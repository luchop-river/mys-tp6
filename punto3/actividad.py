from pedido import Pedido


class Actividad(object):

    def __init__(self, estado):
        self.TAMANIO_DE_ORDEN = 80
        self.estado = estado

    def analizar_stock_diario_contra_umbral(self, dia):
        '''
        Verifica si debe pedir nuevos lotes, siendo stock < umbral y que
        no tengo lotes esperando a que lleguen
        '''
        if (self.estado.debo_reordenar_por_stock_menor_a_umbral()):
            self.__pedir_lote_nuevo(dia)

    def analizar_llegada_de_lotes(self, hoy):
        '''
        Verifica que hayan llegado lotes y actua en respuesta
        '''
        lotes_nuevos = self.__get_lotes_que_llegan_el_dia(hoy)
        if (len(lotes_nuevos) > 0):
            self.estado.ordenes_de_pedidos = (
                self.__get_lotes_que_estan_en_espera_el_dia(hoy)
            )
            self.__procesar_el_arribo_de_lotes(lotes_nuevos)

    # consultas lotes

    def __get_lotes_que_llegan_el_dia(self, dia):
        '''
        CHECKED
        Retorna una lista con los lotes que llegan el dia indicado.
        '''
        return list(
            filter(lambda pedido: pedido.llega_el_dia(dia),
                   self.estado.ordenes_de_pedidos
                   )
        )

    def __get_lotes_que_estan_en_espera_el_dia(self, dia):
        '''
        CHECKED
        Retorna una lista con los lotes que estan en espera el dia indicado
        '''
        return list(
            filter(lambda pedido: pedido.esta_en_espera(dia),
                   self.estado.ordenes_de_pedidos
                   )
        )

    # actividades lotes
    def __procesar_el_arribo_de_lotes(self, lotes_nuevos):
        '''
        CHECKED
        Si los lotes se pueden resolver -> se los procesa
        Sino, se los procesa parcialmente
        '''
        lotes_que_se_pueden_resolver = self.__get_lotes_que_se_pueden_resolver(
            lotes_nuevos
        )
        lotes_que_se_resolveran_parcialmente = (
               self.__get_lotes_que_se_resolveran_parcialmente(lotes_nuevos)
        )
        self.__procesamiento_completo_de_lotes(
            lotes_que_se_pueden_resolver
        )
        self.__procesamiento_parcial_de_lotes(
            lotes_que_se_resolveran_parcialmente
        )

    def __get_lotes_que_se_pueden_resolver(self, lotes_nuevos):
        '''
        CHECKED
        Recive lotes nuevos y retorna los que se pueden procesar el mes actual
        '''
        return list(
            filter(
                lambda pedido: pedido.llega_antes_de_fin_de_mes(),
                lotes_nuevos
            )
        )

    def __get_lotes_que_se_resolveran_parcialmente(self, lotes_nuevos):
        '''
        CHECKED
        Recive lotes y retorna los que no se puden resolver completamente en
        el mes actual
        '''
        return list(
            filter(
                lambda p: not p.llega_antes_de_fin_de_mes(),
                lotes_nuevos
            )
        )

    # procesamiento lotes

    def __procesamiento_completo_de_lotes(self, lotes):
        '''
        CHECKED
        Procesa la llegada de lotes que llegaran en el mes actual:
            * Actualiza inventario y faltante
        '''
        productos_nuevos = len(lotes) * self.TAMANIO_DE_ORDEN
        self.estado.add_cantidad_a_stock_mensual(productos_nuevos)
        if (self.estado.cantidad_actual_de_faltantes_es_mayor_que(
           productos_nuevos)):
            self.estado.decrementar_unidades_faltantes(productos_nuevos)
        else:
            self.estado.actualizar_stock_por_arribo_de_lotes(
                productos_nuevos
            )

    def __procesamiento_parcial_de_lotes(self, lotes):
        '''
        CHECKED
        Procesa la espera parcial de lotes. Llegaran el mes siguiente
        '''
        for lote in lotes:
            lote.esperar_parcialmente()
            self.estado.add_orden_que_llegara_el_mes_siguiente(lote)

    # actividades pedidos

    def atender_demanda_diaria(self, dia, demanda):
        '''
        CHECKED
        Toma acciones dependiendo si se puede satisfacer la demanda diaria con
        el stock actual o no. En caso negativo pide un nuevo lote
        '''
        self.estado.set_demanda_diaria(demanda)
        if (self.estado.tengo_stock_para_soportar_demanda()):
            self.__atender_demanda_diaria_con_suficiente_stock()
        else:
            self.__atender_demanda_diaria_con_stock_faltante(dia)
        self.__actualizar_costos_diarios()

    def __atender_demanda_diaria_con_suficiente_stock(self):
        '''
        CHECKED
        Atiende la demanda del dia de hoy (decrementa inventario y calcula
        mantenimiento de dichos productos)
        Para eso recibo el numero de dia y la demora en dias de la entrega
        '''
        self.estado.actualizar_stock_siendo_suficiente_para_soportar_demanda()
        self.estado.actualizar_costo_de_productos_vendidos()

    def __atender_demanda_diaria_con_stock_faltante(self, dia):
        '''
        CHECKED
        En el caso de que el stock no satisface la demadna diaria, se verifica
        que los lotes que esten por llegar la satisfagan
        '''
        self.estado.add_demanda_a_unidades_faltantes()
        if (self.estado.debo_reordenar_por_faltantes()):
            self.__pedir_lote_nuevo(dia)

    def __actualizar_costos_diarios(self):
        '''
        CHECKED
        Actualizar gastos diarios (contabilizar stock, faltantes y pedidos)
        '''
        self.estado.actualizar_costo_diario_de_mantener_inventario()

    def __pedir_lote_nuevo(self, dia):
        '''
        CHECKED
        Realiza la actividad de pedir un nuevo lote
        '''
        self.estado.add_nueva_orden_menusal()
        pedido = Pedido(dia)
        self.estado.add_nuevo_pedido_de_orden_mensual(pedido)
