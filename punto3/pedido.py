class Pedido(object):
    COSTO_UNITARIO_ORDEN = 28
    DIAS_HABILES_MENSUALES = 30
    TIEMPO_DEMORA_ARRIBO_LOTE = 2

    def __init__(self, inicio):
        self.dia_inicio = inicio
        self.dias_demora = Pedido.TIEMPO_DEMORA_ARRIBO_LOTE

    def llega_el_dia(self, dia):
        '''
        Retorna un booleano dependiendo de si el pedido ya llego o no
        dependiendo el dia que recibe por parametro
        '''
        return (dia >= (self.dia_inicio + self.dias_demora))

    def esta_en_espera(self, dia):
        '''
        Retorna un booleano dependiendo de si el pedido esta en espera o no
        dependiendo el dia que recibe por parametro
        '''
        return (dia < (self.dia_inicio + self.dias_demora))

    def llega_antes_de_fin_de_mes(self):
        '''
        Retorna un booleano dependiendo si el pedido se puede resolver
        en el mes actual
        '''
        return ((self.dia_inicio + self.dias_demora) <
                self.DIAS_HABILES_MENSUALES)

    def esperar_parcialmente(self, dia_inicio_resolucion):
        '''
        Espera parcialmente el pedido, dejando tiempo de llegada para
        el mes que viene
        '''
        dias_espera = self.dia_inicio + self.dias_demora
        dias_laborales = self.DIAS_HABILES_MENSUALES - 1
        self.dia_inicio = 0
        self.dias_demora = (dias_espera - dias_laborales)
