from estado import Estado
import numpy as np
from scipy.stats import poisson
import matplotlib.pyplot as plt
import seaborn as sns

LAMBDA_POISSON = 48
DIAS = 30
MESES = 12
VALORES_UMBRAL_ESTRATEGICOS_DE_LA_SIMULACION = [30, 15, 40]


def main():
    for umbral in VALORES_UMBRAL_ESTRATEGICOS_DE_LA_SIMULACION:
        estado = Estado(umbral)
        simular_anio(estado)
        show_state(estado, umbral)


def simular_anio(estado):
    for _ in range(MESES):
        simular_mes(estado)


def show_state(estado, umbral):
    mostrar_resultados(estado, umbral)
    graficar_resultados(estado, umbral)


def simular_mes(estado):
    for dia in range(DIAS):
        simular_dia(dia, estado)
    finalizar_mes(estado)


def mostrar_resultados(estado, umbral):
    # variables
    stock = estado.get_costos_mensuales_de_almacenar_productos()
    mean_stock = np.mean(stock)
    faltantes = estado.get_costos_mensuales_de_faltantes()
    mean_faltante = np.mean(faltantes)
    ordenes = estado.get_costos_mensuales_de_ordenes()
    mean_ordenes = np.mean(ordenes)
    # ci
    ic_inf_f, ic_sup_f = get_conf_int(faltantes)
    ic_inf_o, ic_sup_o = get_conf_int(ordenes)
    ic_inf_s, ic_sup_s = get_conf_int(stock)
    print('=== Resultados simulaicon anual ===')
    print(f'Umbral stock minimo: {umbral}')
    print(f'Mantenimiento (IC: 95%) [{ic_inf_s:.2f},{ic_sup_s:.2f}] \
        - (mean) {mean_stock:.2f}')
    print(f'Faltante (IC: 95%)  [{ic_inf_f:.2f},{ic_sup_f:.2f}] \
        - (mean) {mean_faltante:.2f}')
    print(f'Ordenes (IC: 95%) [{ic_inf_o:.2f},{ic_sup_o:.2f}] \
        - (mean) {mean_ordenes:.2f} ')


def graficar_resultados(estado, umbral):
    total = get_total_costos(
        estado.get_costos_mensuales_de_almacenar_productos(),
        estado.get_costos_mensuales_de_faltantes(),
        estado.get_costos_mensuales_de_ordenes()
    )
    fig, ax = plt.subplots(nrows=2, ncols=2, figsize=(14, 5))
    ax[0, 0].set(ylabel=f'Cotos de stock', xlabel="Meses de simulación")
    ax[0, 1].set(ylabel=f'Cotos faltantes', xlabel="Meses de simulación")
    ax[1, 0].set(ylabel=f'Cotos de ordenes', xlabel="Meses de simulación")
    ax[1, 1].set(ylabel=f'Cotos Totales', xlabel="Meses de simulación")

    xs = np.arange(1, MESES+1)
    ys1 = np.array(estado.get_costos_mensuales_de_almacenar_productos())
    ys2 = np.array(estado.get_costos_mensuales_de_faltantes())
    ys3 = np.array(estado.get_costos_mensuales_de_ordenes())
    ys4 = np.array(total)

    sns.barplot(x=xs, y=ys1, ax=ax[0, 0])
    sns.barplot(x=xs, y=ys2, ax=ax[0, 1])
    sns.barplot(x=xs, y=ys3, ax=ax[1, 0])
    sns.barplot(x=xs, y=ys4, ax=ax[1, 1])
    fig.tight_layout()
    plt.show()


def simular_dia(dia, estado):
    '''
    Realiza el escaneo de actividades principales:
        * atender la demanda diaria
        * si inventario < umbral && no hay ordenes realizadas then
            realizo pedido reorden
    '''
    estado.escanear_actividades(dia, get_demanda())


def get_conf_int(a, z_value=1.96):
    '''
    Calcula el intervalo de confianza. Formula:
        * desvio tipico = std / square_root(n)
        * d = z_value * desvio tipico
        return [X - d <= mu <= X + d ]
    '''
    n, mean, std = len(a), np.mean(a), np.std(a)
    d = z_value * ((std) / (n ** 0.5))
    return mean - d, mean + d


def get_total_costos(stock, faltante, orden):
    total = []
    for i in range(MESES):
        total.append(stock[i] + faltante[i] + orden[i])
    return total


def finalizar_mes(estado):
    '''
    Calcula costos mensuales e inicializa estado para iniciar un nuevo mes
    '''
    estado.calcular_costos_mensuales()
    estado.iniciar_mes()


def get_demanda():
        return poisson.rvs(LAMBDA_POISSON)
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.poisson.html
        # plots
        # https://docs.scipy.org/doc/numpy/reference/generated/numpy.random.poisson.html


if __name__ == "__main__":
    main()
